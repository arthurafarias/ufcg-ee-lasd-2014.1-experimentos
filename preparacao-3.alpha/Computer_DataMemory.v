module Computer_DataMemory(
	output reg [WORD_WIDTH-1:0] DMEM_out,
	input [WORD_WIDTH-1:0] ADDR_bus_in, DATA_bus_in,
	input [CNTRL_WIDTH-1:0] CNTRL_bus_in,
	input RST
	);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter CNTRL_WIDTH = DR_WIDTH+SB_WIDTH+SA_WIDTH+11;
parameter COUNTER_WIDTH = 4;

reg [WORD_WIDTH-1:0] DMEM [2**DR_WIDTH-1:0];

wire MW = CNTRL_bus_in[3];

reg [2**DR_WIDTH-1:0] i;

initial
	for (i=0;i<2**DR_WIDTH-1;i = i+1)
			DMEM[i] <= 0;

always@(ADDR_bus_in, DATA_bus_in, MW, RST) begin
	if (RST)
		for (i=0;i<2**DR_WIDTH-1;i = i+1)
			DMEM[i] <= 0;
	else if(MW)
		DMEM[ADDR_bus_in] <= DATA_bus_in;
end


always@(*)
	DMEM_out <= DMEM[ADDR_bus_in];
endmodule