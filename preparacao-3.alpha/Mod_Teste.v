module Mod_Teste(
	input CLOCK_27,
	input CLOCK_50,
	input [3:0] KEY,
	input [17:0] SW,
	output [6:0] HEX0,
	output [6:0] HEX1,
	output [6:0] HEX2,
	output [6:0] HEX3,
	output [6:0] HEX4,
	output [6:0] HEX5,
	output [6:0] HEX6,
	output [6:0] HEX7,
	output [8:0] LEDG,
	output [17:0]	LEDR,
	output			LCD_ON,		//	LCD Power ON/OFF
	output			LCD_BLON,	//	LCD Back Light ON/OFF
	output			LCD_RW,		//	LCD Read/Write Select, 0 = Write, 1 = Read
	output			LCD_EN,		//	LCD Enable
	output			LCD_RS,		//	LCD Command/Data Select, 0 = Command, 1 = Data
	inout  [7:0]	LCD_DATA,	//	LCD Data bus 8 bits
////////////////////////	GPIO	////////////////////////////////
	inout  [35:0]	GPIO_0 = 36'hzzzzzzzzz,		//	GPIO Connection 0
	inout  [35:0]	GPIO_1 = 36'hzzzzzzzzz
	);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter CNTRL_WIDTH = DR_WIDTH+SB_WIDTH+SA_WIDTH+11;
parameter COUNTER_WIDTH = 4;

wire RST = KEY[0], CLK = KEY[3];

//	LCD 
assign	LCD_ON		=	1'b1;
assign	LCD_BLON	=	1'b1;
	
Output_LiquidCyrstalDisplay_TestModule LCD0	(	
	//	Host Side
	.iCLK  	( CLOCK_50 ),
	.iRST_N	( KEY[0] ),
	// Data to display
	.d0(PC_status),		// 4 dígitos canto superior esquerdo
	.d1(ADDR_bus),					// 4 dígitos superior meio
	.d2(INSTR_bus),		// 4 dígitos canto superior direito
	.d3(D_bus),			// 4 dígitos canto inferior esquerdo
	.d4(),			// 4 dígitos inferior meio
	.d5(),	// 4 dígitos canto inferior direito
	//	LCD Side
	.LCD_DATA( LCD_DATA ),
	.LCD_RW( LCD_RW ),
	.LCD_EN( LCD_EN ),
	.LCD_RS( LCD_RS )	
	);

Computer CPU0 (
	.COUNTER_bus(PC_status),
	.ADDR_bus(ADDR_bus),
	.DATA_bus(DATA_bus),
	.CLK(KEY[3]),
	.RST(KEY[0])
	);

endmodule