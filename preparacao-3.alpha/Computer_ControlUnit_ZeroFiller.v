module Computer_ControlUnit_ZeroFiller(
	output [WORD_WIDTH-1:0] CONST_bus_out = INSTR_bus_in[2:0],
	input [WORD_WIDTH-1:0] INSTR_bus_in
	);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter CNTRL_WIDTH = DR_WIDTH+SB_WIDTH+SA_WIDTH+11;
parameter COUNTER_WIDTH = 4;

endmodule