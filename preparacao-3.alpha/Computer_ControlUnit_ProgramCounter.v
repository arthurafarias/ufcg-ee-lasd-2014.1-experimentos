module Computer_ControlUnit_ProgramCounter(
	output reg [COUNTER_WIDTH-1:0] COUNTER_out,
	input [WORD_WIDTH-1:0] JMPADDR_in,
	input [CNTRL_WIDTH-1:0] CNTRL_bus_in,
	input [FLAG_WIDTH-1:0] FLAG_bus_in,
	input [WORD_WIDTH-1:0] SE_in,
	input CLK,
	input RST
	);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter CNTRL_WIDTH = DR_WIDTH+SB_WIDTH+SA_WIDTH+11;
parameter COUNTER_WIDTH = 4;
parameter FLAG_WIDTH = 4;

wire PL = CNTRL_bus_in[2];
wire JB = CNTRL_bus_in[1];
wire BC = CNTRL_bus_in[0];

wire N = FLAG_bus_in[1];
wire Z = FLAG_bus_in[0];

always@(posedge CLK, posedge RST)
begin
	case({RST, PL, JB, BC, N, Z})
		6'b1XXXXX: COUNTER_out <= 0;
		5'b011XXX: COUNTER_out <= JMPADDR_in;
		5'b010X1X, 5'b010XX1: COUNTER_out <= COUNTER_out + SE_in;
		default: COUNTER_out <= COUNTER_out + {{(WORD_WIDTH-1){1'b0}},1'b1};
	endcase
end

endmodule