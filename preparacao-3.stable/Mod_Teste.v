module Mod_Teste(
	input CLOCK_27,
	input CLOCK_50,
	input [3:0] KEY,
	input [17:0] SW,
	output [6:0] HEX0,
	output [6:0] HEX1,
	output [6:0] HEX2,
	output [6:0] HEX3,
	output [6:0] HEX4,
	output [6:0] HEX5,
	output [6:0] HEX6,
	output [6:0] HEX7,
	output [8:0] LEDG,
	output [17:0]	LEDR,
	output			LCD_ON,		//	LCD Power ON/OFF
	output			LCD_BLON,	//	LCD Back Light ON/OFF
	output			LCD_RW,		//	LCD Read/Write Select, 0 = Write, 1 = Read
	output			LCD_EN,		//	LCD Enable
	output			LCD_RS,		//	LCD Command/Data Select, 0 = Command, 1 = Data
	inout  [7:0]	LCD_DATA,	//	LCD Data bus 8 bits
////////////////////////	GPIO	////////////////////////////////
	inout  [35:0]	GPIO_0,		//	GPIO Connection 0
	inout  [35:0]	GPIO_1
);

assign	GPIO_1		=	36'hzzzzzzzzz;
assign	GPIO_0		=	36'hzzzzzzzzz;	
	
//	LCD 
assign	LCD_ON		=	1'b1;
assign	LCD_BLON	=	1'b1;
	
LCD_TEST LCD0	(	
	//	Host Side
	.iCLK  	( CLOCK_50 ),
	.iRST_N	( KEY[0] ),
	// Data to display
	.d0(ADDR_bus),		// 4 dígitos canto superior esquerdo
	.d1(DATA_bus),					// 4 dígitos superior meio
	.d2(INSTR_bus),		// 4 dígitos canto superior direito
	.d3(CNTRL_bus),			// 4 dígitos canto inferior esquerdo
	.d4(PC),			// 4 dígitos inferior meio
	.d5(D_bus),	// 4 dígitos canto inferior direito
	//	LCD Side
	.LCD_DATA( LCD_DATA ),
	.LCD_RW( LCD_RW ),
	.LCD_EN( LCD_EN ),
	.LCD_RS( LCD_RS )	
	);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter INSTR_WIDTH = WORD_WIDTH;

wire RST = KEY[0], CLK = KEY[3];

// Program Counter

reg [WORD_WIDTH-1:0] PC;

always@(posedge CLK)
begin
    if(RST == 0) //SE O RESET FOI ATIVADO
		PC <= 0;//ZERA O PC	
    else if((PL)&&(JB)) //JUMP INCONDICIONAL
		PC <= ADDR_bus;//RECEBE O VALOR DE BUS_A QUE DIZ A NOVA POSIÇÃO PRA ONDE VAI PULAR
    else if((PL)&&(!JB)&&(BC)&&(N)) 	//(JUMP) BRANCH ON NEGATIVE
		PC <= PC + {{10{INSTR_bus[8]}}, INSTR_bus[8:6], INSTR_bus[2:0]};//AQUI VÊMOS O EXTEND, OU SEJA,
    else if((PL)&&(!JB)&&(!BC)&&(Z))	//(JUMP) BRANCH ON ZERO						SOMA O PC COM UM NOVO VALOR PRA ELE APONTAR
		PC <= PC + {{10{INSTR_bus[8]}}, INSTR_bus[8:6], INSTR_bus[2:0]};
    else //CASO PADRÃO DO PC
		PC <= PC + 1'b1;
end

// Instruction Memory

reg [INSTR_WIDTH-1:0] INSTR_bus;

always@(PC)
	begin
		case(PC)
			//16'h0000: INSTR_out = 16'b000_0000_000_000_000; //
			16'h0000: INSTR_bus = 16'b100_1100_000_000_011; // LDI  R0 <- 3
			16'h0001: INSTR_bus = 16'b100_1100_001_000_111; // LDI  R1 <- 7
			16'h0002: INSTR_bus = 16'b000_0000_010_000_XXX; // MOVA R2 <- R0
			16'h0003: INSTR_bus = 16'b000_1100_011_XXX_001; // MOVB R3 <- R1
			16'h0004: INSTR_bus = 16'b000_0010_100_000_001; // ADD  R4 <- R0;R1
			16'h0005: INSTR_bus = 16'b000_0101_101_011_100; // SUB  R5 <- R3;R4
			16'h0006: INSTR_bus = 16'b110_0000_000_101_011; // BRZ  R5;3
			16'h0007: INSTR_bus = 16'b110_0001_000_101_011; // BRN  R5;3
			16'h000A: INSTR_bus = 16'b111_0000_110_000_001; // JMP  R0;
		endcase
	end

// Instruction Decoder

wire [3*DR_WIDTH-1+11:0] CNTRL_bus;

wire PL = CNTRL_bus[2];
wire JB = CNTRL_bus[1];
wire BC = CNTRL_bus[0];

wire [OPCODE_WIDTH-1:0] OPCODE = INSTR_bus[15:9];

assign CNTRL_bus[0] = INSTR_bus[9];
assign CNTRL_bus[1] = INSTR_bus[13];
assign CNTRL_bus[2] = INSTR_bus[15]&INSTR_bus[14];
assign CNTRL_bus[3] = ~INSTR_bus[15]&INSTR_bus[14];
assign CNTRL_bus[4] = ~INSTR_bus[14];
assign CNTRL_bus[5] = INSTR_bus[13];
assign CNTRL_bus[9:6] = {INSTR_bus[12:10],~CNTRL_bus[2]&INSTR_bus[9]};
assign CNTRL_bus[10] = INSTR_bus[15];
assign CNTRL_bus[13:11] = INSTR_bus[2:0];
assign CNTRL_bus[16:14] = INSTR_bus[5:3];
assign CNTRL_bus[19:17] = INSTR_bus[8:6];

// Register file

reg [WORD_WIDTH-1:0] ADDR_bus;
reg [WORD_WIDTH-1:0] B_data;
wire RW = CNTRL_bus[4];

wire [SA_WIDTH-1:0] DA = CNTRL_bus[19:17];
wire [SA_WIDTH-1:0] AA = CNTRL_bus[16:14];
wire [SA_WIDTH-1:0] BA = CNTRL_bus[13:11];

reg [WORD_WIDTH-1:0] SYNC_RAM0 [2**DR_WIDTH-1:0];
reg [WORD_WIDTH-1:0] SYNC_RAM1 [2**DR_WIDTH-1:0];

always@(posedge CLK) begin
	if(RW) begin
		SYNC_RAM0[DA] <= D_bus;
		SYNC_RAM1[DA] <= D_bus;
	end
end

always@(*) begin
	ADDR_bus <= SYNC_RAM0[AA];
	B_data <= SYNC_RAM1[BA];
end

/*
reg [WORD_WIDTH-1:0] REGS[7:0];

always@(posedge CLK)
	if(RW)
		case(DA)
			3'b 000 : REGS[0] <= D_bus;
			3'b 001 : REGS[1] <= D_bus;
			3'b 010 : REGS[2] <= D_bus;
			3'b 011 : REGS[3] <= D_bus;
			3'b 100 : REGS[4] <= D_bus;
			3'b 101 : REGS[5] <= D_bus;
			3'b 110 : REGS[6] <= D_bus;
			3'b 111 : REGS[7] <= D_bus;
		endcase
		
always@(AA, BA, REGS)
	begin
		case(AA)
			3'b 000 : ADDR_bus <= REGS[0];
			3'b 001 : ADDR_bus <= REGS[1];
			3'b 010 : ADDR_bus <= REGS[2];
			3'b 011 : ADDR_bus <= REGS[3];
			3'b 100 : ADDR_bus <= REGS[4];
			3'b 101 : ADDR_bus <= REGS[5];
			3'b 110 : ADDR_bus <= REGS[6];
			3'b 111 : ADDR_bus <= REGS[7];
		endcase
		
		case(BA)
			3'b 000 : B_data <= REGS[0];
			3'b 001 : B_data <= REGS[1];
			3'b 010 : B_data <= REGS[2];
			3'b 011 : B_data <= REGS[3];
			3'b 100 : B_data <= REGS[4];
			3'b 101 : B_data <= REGS[5];
			3'b 110 : B_data <= REGS[6];
			3'b 111 : B_data <= REGS[7];
		endcase
	end
*/
// B Muxer

reg [WORD_WIDTH-1:0] DATA_bus;

wire MB = CNTRL_bus[10];

always@(MB, B_data, INSTR_bus)
	case(MB)
		1'b0: DATA_bus <= B_data;
		1'b1: DATA_bus <= INSTR_bus[2:0];
	endcase

// Arithmetic  Logic Unit

parameter FS_WIDTH = 4;

reg [WORD_WIDTH-1:0] F;
wire [WORD_WIDTH-2:0] V_temp;
wire [FS_WIDTH-1:0] FS = CNTRL_bus[9:6];
wire V, N, Z;
reg C;

assign V_temp = ADDR_bus[WORD_WIDTH-2:0]+DATA_bus[WORD_WIDTH-2:0];
assign V = V_temp[WORD_WIDTH-2:0];
assign N = F[WORD_WIDTH-1];
assign Z = (!F)?1'b1:1'b0;

always@(*)
	case(FS)
		4'b0000: {C, F} = ADDR_bus;					// Move A
		4'b0001: {C, F} = ADDR_bus+1;					// Increment
		4'b0010: {C, F} = ADDR_bus+DATA_bus;		// Add
		4'b0011: {C, F} = ADDR_bus+DATA_bus+1;		// 
		4'b0100: {C, F} = ADDR_bus+(~DATA_bus);	// 
		4'b0101: {C, F} = ADDR_bus+(~DATA_bus)+1;	// Subtraction
		4'b0110: {C, F} = ADDR_bus-1;					// Decrement
		4'b0111: {C, F} = ADDR_bus;					// Move A
		4'b1000: {C, F} = ADDR_bus&DATA_bus;		// Bitwize and
		4'b1001: {C, F} = ADDR_bus|DATA_bus;		// Bitwize or
		4'b1010: {C, F} = ADDR_bus^DATA_bus;		// Bitwize xor
		4'b1011: {C, F} = (~ADDR_bus);				// Bitwize Invert
		4'b1100: {C, F} = DATA_bus;					// Move B
		4'b1101: {C, F} = (DATA_bus>>1);				// Shift Right B
		4'b1110: {C, F} = (DATA_bus<<1);				// Shift Left B
		4'b1111: {C, F} = (~DATA_bus);				// Ivert B
	endcase

	
// Data Memory

reg [WORD_WIDTH-1:0] DMEM [2**DR_WIDTH-1:0];
reg [WORD_WIDTH-1:0] DMEM_out;

wire MW = CNTRL_bus[3];

always@(ADDR_bus, DATA_bus, MW) begin
	if(MW)
		DMEM[ADDR_bus] <= DATA_bus;
end


always@(*)
DMEM_out <= DMEM[ADDR_bus];

// D Muxer

reg [WORD_WIDTH-1:0] D_bus;

wire MD = CNTRL_bus[5];

always@(F, DMEM_out, MD)
	case(MD)
		1'b0: D_bus = F;
		1'b1: D_bus = DMEM_out;
	endcase
	
endmodule