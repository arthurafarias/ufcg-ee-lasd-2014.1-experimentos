UFCG-EE-LASD-2014.1-Experiments
===============================

Estados:	alpha -> Em desenvolvimento, não testado
		stable -> Funcionando e Testado
		depracated -> Abandonado

UFCG - Engenharia Elétrica - Laboratório de Arquitetura de Sistemas Digitais (2014.1): Experimentos
Instruções

	Formato da Instrução [16bit]
	OPCODE[7bit]; DR[3bit]; SA[3bit]; SB[3bit].

	R[SX] Seleciona o registrador SX no banco de registradores R do datapath e põe na saída X
	AND(A,B) Realiza a operação bitwise entre A e B
	M[A] Seleciona o registrador A na memórida de dados M e põe na entrada externa do datapath
	ZF(A) completa com zeros o valor A
	EX(A) completa com o último algarismo significativo de A

	Código		Nome	Operação
	0000000		MOVA	R[DR] <- R[SA]
	0000001		INC		R[DR] <- R[SA]+1
	0000010		ADD 	R[DR] <- R[SA]+R[SB]
	0000101		SUB 	R[DR] <- R[SA]-R[SB]
	0000110		DEC 	R[DR] <- R[SA]-1
	0001000		AND 	R[DR] <- AND(R[SA],R[SB])
	0001001		OR 		R[DR] <- OR(R[SA],R[SB])
	0001010		XOR 	R[DR] <- XOR(R[SA],R[SB])
	0001011		INVA 	R[DR] <- ~R[SA]
	0001100		MOVB 	R[DR] <- R[SB]
	0001101		SHRB 	R[DR] <- R[SB]>>1
	0001110		SHLB 	R[DR] <- R[SB]<<1
	1001100		LDI 	R[DR] <- ZF(SB)
	1000010		ADI 	R[DR] <- R[SA]+ZF(SB)
	0010000		LDA		R[DR] <- M[SA]
	0100000		STB 	M[SA] <- R[SB]
	1100000		BRZ 	if ( R[SA] == 0 ) { PC = PC + EX(DR||SB) } else { PC = PC + 1 }
	1100001		BRN		if (R[SA] < 0 ) { PC = PC + EX(DR||SB) } else { PC = PC + 1 }
	1110000		JMP 	PC <- R[SA]

	Código				Palavra de Controle
	0000000				00000010000
	0000001				00000010001
	0000010				00010010000
	0000101				00100010001
	0000110				00110010000
	0001000				01000010000
	0001001				01000010001
	0001010				01010010000
	0001011				01010010001
	0001100				01100010000
	0001101				01100010001
	0001110				01110010000
	1001100				11100010000
	1000010				10010010000
	0010000				00000110010
	0100000				00000001000
	1100000				10000000100
	1100001				10001000101
	1110000				10000100110
