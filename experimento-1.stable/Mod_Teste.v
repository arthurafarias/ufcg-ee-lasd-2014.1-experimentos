module Mod_Teste( input CLOCK_27, input CLOCK_50, input [3:0] KEY, input [17:0] SW, output [6:0] HEX0, output [6:0] HEX1, output [6:0] HEX2, output [6:0] HEX3, output [6:0] HEX4, output [6:0] HEX5, output [6:0] HEX6, output [6:0] HEX7, output [8:0] LEDG, output [17:0] LEDR);

/////////////////////////////
//LAB01 22/05 - Atividade 2//
/////////////////////////////

wire [15:0] A_Data, B_Data;

RegisterFile RF0(
	.D_Data({{8{SW[15]}},{4{SW[14]}},SW[13],SW[12],SW[11],SW[10]}),
	.AA(SW[3:1]),
	.BA(SW[6:4]),
	.DA(SW[9:7]),
	.RW(SW[0]),
	.CLK(KEY[3]),
	.A_Data(A_Data),
	.B_Data(B_Data),
);

Hexa7seg CONV0(.in(A_Data[3:0]), .out(HEX0));
Hexa7seg CONV1(.in(A_Data[7:4]), .out(HEX1));
Hexa7seg CONV2(.in(A_Data[11:8]), .out(HEX2));
Hexa7seg CONV3(.in(A_Data[15:12]), .out(HEX3));
Hexa7seg CONV4(.in(B_Data[3:0]), .out(HEX4));
Hexa7seg CONV5(.in(B_Data[7:4]), .out(HEX5));
Hexa7seg CONV6(.in(B_Data[11:8]), .out(HEX6));
Hexa7seg CONV7(.in(B_Data[15:12]), .out(HEX7));

endmodule


	
