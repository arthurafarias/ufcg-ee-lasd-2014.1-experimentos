module Computer_Datapath_BusMuxer(
	output reg [WORD_WIDTH-1:0] BUS_out,
	input [WORD_WIDTH-1:0] A_in, B_in,
	input S
	);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter CNTRL_WIDTH = DR_WIDTH+SB_WIDTH+SA_WIDTH+11;
parameter COUNTER_WIDTH = 4;

always@(*)
	case(S)
		1'b0: BUS_out <= A_in;
		1'b1: BUS_out <= B_in;
	endcase

endmodule