module LCDhex (
		////////////////////	Clock Input	 	////////////////////	 
		CLOCK_27,							//	27 MHz
		CLOCK_50,							//	50 MHz
		////////////////////	Push Button		////////////////////
		KEY,							//	Button[3:0]
		////////////////////	DPDT Switch		////////////////////
		SW,						//	DPDT Switch[17:0]
		////////////////////	7-SEG Dispaly	////////////////////
		HEX0,							//	Seven Segment Digital 0
		HEX1,							//	Seven Segment Digital 1
		HEX2,							//	Seven Segment Digital 2
		HEX3,							//	Seven Segment Digital 3
		HEX4,							//	Seven Segment Digital 4
		HEX5,							//	Seven Segment Digital 5
		HEX6,							//	Seven Segment Digital 6
		HEX7,							//	Seven Segment Digital 7
		////////////////////////	LED		////////////////////////
		LEDG,						//	LED Green[8:0]
		LEDR,						//	LED Red[17:0]
		////////////////////////	UART	////////////////////////
		UART_TXD,						//	UART Transmitter
		UART_RXD,						//	UART Rceiver
		////////////////////////	IRDA	////////////////////////
		////////////////////	LCD Module 16X2		////////////////
		LCD_ON,							//	LCD Power ON/OFF
		LCD_BLON,						//	LCD Back Light ON/OFF
		LCD_RW,							//	LCD Read/Write Select, 0 = Write, 1 = Read
		LCD_EN,							//	LCD Enable
		LCD_RS,							//	LCD Command/Data Select, 0 = Command, 1 = Data
		LCD_DATA,						//	LCD Data bus 8 bits
		GPIO_0,							// GPIO
		GPIO_1			
	);

////////////////////////	Clock Input	 	////////////////////////
	input			CLOCK_27;					//	27 MHz
	input			CLOCK_50;					//	50 MHz
////////////////////////	Push Button		////////////////////////
	input	[3:0]	KEY;					//	Button[3:0]
////////////////////////	DPDT Switch		////////////////////////
	input	[17:0]	SW;				//	DPDT Switch[17:0]
////////////////////////	7-SEG Dispaly	////////////////////////
	output	[6:0]	HEX0;					//	Seven Segment Digital 0
	output	[6:0]	HEX1;					//	Seven Segment Digital 1
	output	[6:0]	HEX2;					//	Seven Segment Digital 2
	output	[6:0]	HEX3;					//	Seven Segment Digital 3
	output	[6:0]	HEX4;					//	Seven Segment Digital 4
	output	[6:0]	HEX5;					//	Seven Segment Digital 5
	output	[6:0]	HEX6;					//	Seven Segment Digital 6
	output	[6:0]	HEX7;					//	Seven Segment Digital 7
////////////////////////////	LED		////////////////////////////
	output	[8:0]	LEDG;				//	LED Green[8:0]
	output	[17:0]	LEDR;				//	LED Red[17:0]
////////////////////////////	UART	////////////////////////////
	output			UART_TXD;				//	UART Transmitter
	input			UART_RXD;				//	UART Rceiver
////////////////////	LCD Module 16X2	////////////////////////////
	inout	[7:0]	LCD_DATA;				//	LCD Data bus 8 bits
	output			LCD_ON;					//	LCD Power ON/OFF
	output			LCD_BLON;				//	LCD Back Light ON/OFF
	output			LCD_RW;					//	LCD Read/Write Select, 0 = Write, 1 = Read
	output			LCD_EN;					//	LCD Enable
	output			LCD_RS;					//	LCD Command/Data Select, 0 = Command, 1 = Data
////////////////////////	GPIO	////////////////////////////////
	inout	[35:0]	GPIO_0;					//	GPIO Connection 0
	inout	[35:0]	GPIO_1;					//	GPIO Connection 1
////////////////////////////////////////////////////////////////////

//	All inout port turn to tri-state	
	assign	GPIO_1		=	36'hzzzzzzzzz;
	assign	GPIO_0		=	36'hzzzzzzzzz;	
	
////////////////////////////////////////////////////////////////////

	reg [15:0] d0,d1,d2,d3,d4;
	
	always @(posedge KEY[2])
	   case(SW[17:16])
         0: d0 <= SW[15:0];
         1: d1 <= SW[15:0];
         2: d2 <= SW[15:0];
         default: d3 <= SW[15:0];
       endcase
	
	always @(posedge CLOCK_27)
	   if(!KEY[0]) begin
         d4 <= 0;
       end
       else begin
         d4 <= d4+d0;
       end

	//	LCD 
	assign	LCD_ON		=	1'b1;
	assign	LCD_BLON	=	1'b1;
	
	LCD_TEST 			u5	(	
	
							//	Host Side
							
							.iCLK  	( CLOCK_50 ),
							.iRST_N	( KEY[0] ),
							
							// Data to display
							.d0 (d0),
							.d1 (d1),
							.d2 (d2),
							.d3 (d3),
							.d4 (d4),
							.d5 (SW[15:0]),
							
							//	LCD Side
							
							.LCD_DATA( LCD_DATA ),
							.LCD_RW  ( LCD_RW ),
							.LCD_EN	 ( LCD_EN ),
							.LCD_RS  ( LCD_RS )	
							
							);
							
	/*assign HEX3=7'b0101011;
	assign HEX2=7'b0001000;
	assign HEX1=7'b0000111;
	assign HEX0=7'b0100011;
	assign HEX4=7'b1111111;
	assign HEX5=7'b1111111;
	assign HEX6=7'b1111111;
	assign HEX7=7'b1111111;*/
				
endmodule
