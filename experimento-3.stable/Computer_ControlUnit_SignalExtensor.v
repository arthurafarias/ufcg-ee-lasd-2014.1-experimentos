module Computer_ControlUnit_SignalExtensor(
	output [WORD_WIDTH-1:0] SE_out,
	input [WORD_WIDTH-1:0] INSTR_bus_in
	);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter CNTRL_WIDTH = DR_WIDTH+SB_WIDTH+SA_WIDTH+11;
parameter COUNTER_WIDTH = 4;

assign SE_out = {{10{INSTR_bus_in[3*DR_WIDTH-1]}}, INSTR_bus_in[3*DR_WIDTH-1:2*DR_WIDTH], INSTR_bus_in[DR_WIDTH-1:0]};

endmodule