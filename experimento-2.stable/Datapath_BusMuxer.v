module Datapath_BusMuxer(
	output reg [WORD_WIDTH-1:0] out,
	input [WORD_WIDTH-1:0] A_in, B_in,
	input S
	);

parameter WORD_WIDTH = 16;

always@(*)
	case(S)
		1'b0: out = A_in;
		1'b1: out = B_in;
	endcase
	
endmodule
