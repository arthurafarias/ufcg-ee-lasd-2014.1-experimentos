module Datapath_RegisterFile(
	output reg [WORD_WIDTH-1:0] A_data, B_data,
	input [WORD_WIDTH-1:0] D_data,
	input [DR_WIDTH-1:0] AA, BA, DA,
	input RW, CLK
	);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter INSTR_WIDTH = WORD_WIDTH;

reg [WORD_WIDTH-1:0] A_data;
reg [WORD_WIDTH-1:0] B_data;
wire RW = CNTRL_bus[4];

wire [SA_WIDTH-1:0] DA = CNTRL_bus[19:17];
wire [SA_WIDTH-1:0] AA = CNTRL_bus[16:14];
wire [SA_WIDTH-1:0] BA = CNTRL_bus[13:11];

reg [WORD_WIDTH-1:0] SYNC_RAM0 [2**DR_WIDTH-1:0];
reg [WORD_WIDTH-1:0] SYNC_RAM1 [2**DR_WIDTH-1:0];

always@(posedge CLK) begin
	if(RW) begin
		SYNC_RAM0[DA] <= D_bus;
		SYNC_RAM1[DA] <= D_bus;
	end
end

always@(*) begin
	ADDR_bus <= SYNC_RAM0[AA];
	B_data <= SYNC_RAM1[BA];
end

endmodule	
