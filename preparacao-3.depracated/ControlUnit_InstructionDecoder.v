module ControlUnit_InstructionDecoder(output [CNTRL_WIDTH-1:0] CNTRL_out, input [INSTR_WIDTH-1:0] INSTR_in);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter INSTR_WIDTH = WORD_WIDTH;
parameter CNTRL_WIDTH = 3*DR_WIDTH+11;

assign CNTRL_out[0] = INSTR_in[9];
assign CNTRL_out[1] = INSTR_in[13];
assign CNTRL_out[2] = INSTR_in[14]&INSTR_in[15];
assign CNTRL_out[3] = INSTR_in[14]&(~INSTR_in[15]);
assign CNTRL_out[4] = ~INSTR_in[14];
assign CNTRL_out[5] = INSTR_in[13];
assign CNTRL_out[9:6] = {(~INSTR_in[2])&INSTR_in[9], INSTR_in[12:10]};
assign CNTRL_out[10] = INSTR_in[15];
assign CNTRL_out[SB_WIDTH-1+11:11] = INSTR_in[SB_WIDTH-1:0];
assign CNTRL_out[SA_WIDTH-1+SB_WIDTH+11:SB_WIDTH+11] = INSTR_in[SA_WIDTH-1+SB_WIDTH:SB_WIDTH];
assign CNTRL_out[DR_WIDTH-1+SA_WIDTH+SB_WIDTH+11:SA_WIDTH+SB_WIDTH+11] = INSTR_in[DR_WIDTH-1+SA_WIDTH+SB_WIDTH:SA_WIDTH+SB_WIDTH];

endmodule