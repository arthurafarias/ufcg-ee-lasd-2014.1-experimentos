module ControlUnit_ZeroFiller(output [WORD_WIDTH-1:0] CONST_out, input [INSTR_WIDTH-1:0] INSTR_in);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter INSTR_WIDTH = WORD_WIDTH;

assign CONST_out = INSTR_in[2:0];

endmodule