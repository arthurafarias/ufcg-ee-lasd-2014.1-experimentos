module InstructionMemory(output reg[WORD_WIDTH-1:0] INSTR_out, input[WORD_WIDTH-1:0] INSTR_in);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter INSTR_WIDTH = WORD_WIDTH;
/*
0000		LDI R0,3
0001		LDI R1,7
0002		MOVA R2, R0
0003		MOVB R3, R1
0004		ADD R4,R0,R1
0005		SUB R5,R3,R4
0006		BRZ R5, 3
0007		BRN R5, 3
000A		JMP R0
*/
always@(*)
	begin
		case(INSTR_in)
			//16'h0000: INSTR_out = 16'b000_0000_000_000_000; //
			16'h0000: INSTR_out = 16'b100_1100_000_000_011; // LDI  R0 <- 3
			16'h0001: INSTR_out = 16'b100_1100_001_000_111; // LDI  R1 <- 7
			16'h0002: INSTR_out = 16'b000_0000_010_010_XXX; // MOVA R2 <- R0
			16'h0003: INSTR_out = 16'b000_1100_011_011_XXX; // MOVB R3 <- R1
			16'h0004: INSTR_out = 16'b000_0010_010_000_001; // ADD  R4 <- R0;R1
			16'h0005: INSTR_out = 16'b000_0101_101_011_100; // SUB  R5 <- R3;R4
			16'h0006: INSTR_out = 16'b110_0000_000_101_011; // BRZ  R5;3
			16'h0007: INSTR_out = 16'b110_0001_000_101_011; // BRN  R5;3
			16'h0008: INSTR_out = 16'b111_0000_110_000_001; // JMP  R0;
			default: INSTR_out = 16'b0;
		endcase
	end
/*
always@(*)
	begin			//de acordo com a posição que o PC apontar, execute determinada instrução,
    case(INSTR_in)	//que basicamente é a que está salva naquela memória
        16'h0000: INSTR_out = 16'b 0000000_001_010_XXX; // mova R1, R2 - NÃO TEM PROBLEMA DE DEIXAR O XXX OU 000.
        16'h0001: INSTR_out = 16'b 0000001_110_100_001; // inc R6, R4
        16'h0002: INSTR_out = 16'b 0000010_011_001_110; // add R3, R1, R6
        16'h0003: INSTR_out = 16'b 0000101_010_101_010; // sub R2, R5, R2
        16'h0004: INSTR_out = 16'b 0000110_000_111_001; // dec R0, R7
        16'h0005: INSTR_out = 16'b 0001000_101_011_010; // and R5, R3, R2
        16'h0006: INSTR_out = 16'b 0001001_000_100_110; // or R0, R4, R6
        16'h0007: INSTR_out = 16'b 0001010_111_001_010; // xor R7, R1, R2
        16'h0008: INSTR_out = 16'b 0001011_000_011_010; // not R0, R3
        16'h0009: INSTR_out = 16'b 0001100_010_000_100; // movb R2, R4
        16'h000A: INSTR_out = 16'b 0001101_001_100_011; // shr R1, R3
        16'h000B: INSTR_out = 16'b 0001110_111_101_110; // shl R7, R6
        16'h000C: INSTR_out = 16'b 1001100_010_000_010; // ldi R2, 2
        16'h000D: INSTR_out = 16'b 1000010_100_011_100; // adi R4, R3, 4
        16'h000E: INSTR_out = 16'b 0010000_101_000_010; // ld R5, R0
        16'h000F: INSTR_out = 16'b 0100000_000_001_111; // st R1, R7
        16'h0010: INSTR_out = 16'b 1100000_000_100_011; // brz R4, 3
        16'h0011: INSTR_out = 16'b 1100001_111_010_000; // brn R2, 8
        16'h0012: INSTR_out = 16'b 1110000_110_111_001; // jmp R7
        default:  INSTR_out = 16'b 1110000_110_000_001;
    endcase
end //fim do always referente a memória de instruções
*/

endmodule