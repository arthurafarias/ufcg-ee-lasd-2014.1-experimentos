module ControlUnit_ProgramCounter(
	output reg [INSTR_WIDTH-1:0] INSTR_out,
	input [INSTR_WIDTH-1:0] JMPADDR_in,
	input [7:0] INSTR_in,
	input [INSTR_WIDTH-1:0] EXTNDR_in,
	input CLK
	);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter INSTR_WIDTH = WORD_WIDTH;

wire PL, JB, BC, N, Z, RST;

assign PL = INSTR_in[7];
assign JB = INSTR_in[6];
assign BC = INSTR_in[5];

assign N = INSTR_in[2];
assign Z = INSTR_in[1];

assign RST = INSTR_in[0];

reg [WORD_WIDTH-1:0] PC;

always@(posedge CLK)
begin
    if((PL)&&(JB))
		PC <= JMPADDR_in;
    else if((PL)&&(!JB)&&(BC)&&(N))
		PC <= PC + {{10{INSTR_in[8]}}, INSTR_in[8:6], INSTR_in[2:0]};
    else if((PL)&&(!JB)&&(!BC)&&(Z))
		PC <= PC + {{10{INSTR_in[8]}}, INSTR_in[8:6], INSTR_in[2:0]};
    else
		PC <= PC + 1'b1;
end

always@(posedge CLK)
	if (RST) == 0
		PC <= 0;

endmodule