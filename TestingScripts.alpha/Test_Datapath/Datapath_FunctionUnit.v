module Datapath_FunctionUnit(
	output [WORD_WIDTH-1:0] F,
	output V, C, N, Z,
	input [WORD_WIDTH-1:0] A,
	input [WORD_WIDTH-1:0] B,
	input [3:0] FS
);
/*
* F - Function unit output data Size: WORD_WIDTH
* A - Function unit input data A Size: WORD_WIDTH
* B - Function unit input data B Size: WORD_WIDTH
* FS - Function unit function selector Size: 4 bit
* N - Negative Output Flag Size: 1 bit
* Z - Zero Flag Size: 1 bit
* C - Carry Flag - Overflow without signal Size: 1 bit
* V - Overflow Flag - Overflow with signal Size: 1 bit
*/

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter INSTR_WIDTH = WORD_WIDTH;

wire [WORD_WIDTH-1:0] V_temp;
assign V_temp = A[WORD_WIDTH-2:0]+B[WORD_WIDTH-2:0];
assign V = V_temp[WORD_WIDTH-1];
assign N = F[WORD_WIDTH-1];
assign Z = (!F)?1'b1:1'b0;

Datapath_FunctionUnit_ArithmeticLogicUnit_Behavioral ULA0(F,C,A,B,FS);

defparam ULA0.WORD_WIDTH = WORD_WIDTH;
defparam ULA0.DR_WIDTH = DR_WIDTH;
	
endmodule
