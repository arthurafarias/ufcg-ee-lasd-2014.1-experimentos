module Datapath_BusMuxer(
	output reg [WORD_WIDTH-1:0] out,
	input [WORD_WIDTH-1:0] A_in, B_in,
	input S
	);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter INSTR_WIDTH = WORD_WIDTH;

always@(*)
	case(S)
		1'b0: out = A_in;
		1'b1: out = B_in;
	endcase
	
endmodule
