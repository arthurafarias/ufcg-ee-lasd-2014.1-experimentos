module Test_Datapath_BuxMuxer(
	SW[17:0],
	LEDR[17:0],
	);

wire A = SW[15:0];
wire B = ~A;

Datapath_BusMuxer BM(
	.out(LEDR),
	.A_in(A),
	.B_in(B),
	.S(SW[17])
	);

endmodule