module Test_ControlUnit(
	input SW[17:0],
	input KEY[3:0],
	output LEDR[17:0],
	output LEDG[8:0]
	);

reg CLK, KEY;

module ControlUnit(
	CNTRL_out(CNTRL_out),
	PC_out(PC_out),
	CONST_out(CONST_out),
	INSTR_in(INSTR_in),
	INSTRM_in(INSTRM_in),
	FLAG_in(FLAG_in),
	RST_in(RST_in),
	CLK_in(CLK_in)
	);


	// Control Unit Test

endmodule