module ControlUnit_BranchController(
	output [7:0] BC_out,
	input [CNTRL_WIDTH-1:0] CNTRL_in,
	input [3:0] FLAG_in,
	input RST
	);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
//parameter INSTR_WIDTH = WORD_WIDTH;
parameter CNTRL_WIDTH = 3*DR_WIDTH+11;

assign BC_out = {CNTRL_in[2:0], FLAG_in, RST};

endmodule