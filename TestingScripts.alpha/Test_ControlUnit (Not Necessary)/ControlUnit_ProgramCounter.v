module ControlUnit_ProgramCounter(
	output reg [INSTR_WIDTH-1:0] INSTR_out,
	input [INSTR_WIDTH-1:0] INSTR_in,
	input [7:0] BC_in,
	input [INSTR_WIDTH-1:0] EXTNDR_in,
	input CLK
	);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter INSTR_WIDTH = WORD_WIDTH;

//wire PL, JB, BC, V, C, N, Z, RST;
wire PL, JB, BC, N, Z, RST;

assign PL = BC_in[7];
assign JB = BC_in[6];
assign BC = BC_in[5];
//assign V = BC_in[4];
//assign C = BC_in[3];
assign N = BC_in[2];
assign Z = BC_in[1];
assign RST = BC_in[0];

always@(posedge CLK) begin
	if (RST) INSTR_out <= 0;
	else if (PL&&JB) INSTR_out <= INSTR_in;
	else if (PL&&!JB&&BC&&N) INSTR_out <= EXTNDR_in;
	else if (PL&&!JB&&!BC&&Z) INSTR_out <= EXTNDR_in;
	else INSTR_out <= INSTR_out+1'b1;
end

endmodule