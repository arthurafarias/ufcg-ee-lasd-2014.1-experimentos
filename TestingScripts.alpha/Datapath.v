module  Datapath(
	output [3:0] FLAG_out,					// Flag Bus
	output [WORD_WIDTH-1:0] A_bus, 		// Address out Bus
	output [WORD_WIDTH-1:0] D_bus, 		// Data out Bus
	output [WORD_WIDTH-1:0] D_bus_internal, // Internal data bus output for monitoring
	input [WORD_WIDTH-1:0] D_in,  		// Data in bus
	input [CNTRL_WIDTH-1:0] CNTRL_in, 					// Instruction Bus
	input [WORD_WIDTH-1:0] CONST_in,		// Constant In Bus
	input CLK
);

assign D_bus_internal = BMD_out;

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter CNTRL_WIDTH = SB_WIDTH+SA_WIDTH+DR_WIDTH+11;

wire [WORD_WIDTH-1:0] RF0_out_B, BMD_out, FU0_out;

Datapath_RegisterFile RF0(
	.A_data(A_bus),
	.B_data(RF0_out_B),
	.D_data(BMD_out),
	.BA(CNTRL_in[SB_WIDTH-1+11:11]),
	.AA(CNTRL_in[SA_WIDTH-1+SB_WIDTH+11:SB_WIDTH+11]),
	.DA(CNTRL_in[DR_WIDTH-1+SB_WIDTH+SA_WIDTH+11:SB_WIDTH+SA_WIDTH+11]),
	.RW(CNTRL_in[0]),
	.CLK(CLK)
	);

defparam RF0.WORD_WIDTH = WORD_WIDTH;
defparam RF0.DR_WIDTH = DR_WIDTH;

Datapath_BusMuxer BMB(
	.out(D_bus),
	.A_in(RF0_out_B),
	.B_in(CONST_in),
	.S(CNTRL_in[6])
	);

defparam BMB.WORD_WIDTH = WORD_WIDTH;
defparam BMB.DR_WIDTH = DR_WIDTH;
	
Datapath_BusMuxer BMD(
	.out(BMD_out),
	.A_in(FU0_out),
	.B_in(D_in),
	.S(CNTRL_in[1])
	);

defparam BMD.WORD_WIDTH = WORD_WIDTH;
defparam BMD.DR_WIDTH = DR_WIDTH;
	
Datapath_FunctionUnit FU0(
	.F(FU0_out),
	.V(FLAG_out[3]),
	.C(FLAG_out[2]),
	.N(FLAG_out[1]),
	.Z(FLAG_out[0]),
	.A(A_bus),
	.B(D_bus),
	.FS(CNTRL_in[9:6])
	);

defparam FU0.WORD_WIDTH = WORD_WIDTH;
defparam FU0.DR_WIDTH = DR_WIDTH;

endmodule