module DataMemory(output reg [WORD_WIDTH-1:0] Data_out, input [WORD_WIDTH-1:0] Addr_in, Data_in, input [CNTRL_WIDTH-1:0] CNTRL_in);

parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter OPCODE_WIDTH = 7;
parameter INSTR_WIDTH = WORD_WIDTH;
parameter CNTRL_WIDTH = 3*DR_WIDTH+11;

reg [WORD_WIDTH-1:0] RAM[(2**5)-1:0];

always@(*) begin
	if (CNTRL_in[3]) 
		RAM[Addr_in] <= Data_in;
	
	Data_out <= RAM[Addr_in[4:0]];
end

endmodule
