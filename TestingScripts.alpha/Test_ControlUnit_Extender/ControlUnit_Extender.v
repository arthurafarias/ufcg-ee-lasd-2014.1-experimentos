module ControlUnit_Extender(
	output [WORD_WIDTH-1:0] EXTNDR_out,
	input [INSTR_WIDTH-1:0] INSTR_in
	);
	
parameter WORD_WIDTH = 16;
parameter DR_WIDTH = 3;
parameter SB_WIDTH = DR_WIDTH;
parameter SA_WIDTH = DR_WIDTH;
parameter OPCODE_WIDTH = 7;
parameter INSTR_WIDTH = WORD_WIDTH;

assign EXTNDR_out = {
	{(INSTR_WIDTH-6){INSTR_in[3*DR_WIDTH-1]}},
	INSTR_in[3*DR_WIDTH-1:6],
	INSTR_in[DR_WIDTH-1:0]};

endmodule