module Test_ControlUnit_BranchControler(
	input [17:0]SW,
	input [3:0]KEY,
	output [17:0]LEDR,
	output [8:0]LEDG
	);

wire [7:0] BC_out;
reg [15:0] CNTRL_in;
reg [3:0] FLAG_in;

assign LEDR[7:0] = BC_out;
wire RST = KEY[0];

ControlUnit_BranchController BC(
	.BC_out(BC_out), // 8bit
	.CNTRL_in(CNTRL_in), // 16bit
	.FLAG_in(FLAG_in), // 4bit
	.RST(RST) // 1bit
	);

always@(SW[0]) begin
	case(SW[0])
		//2'b00: BC_out <= SW[10:3];
		1'b0: CNTRL_in <= SW[16:1];
		1'b1: FLAG_in <= SW[4:1];
		2'b11: ;
		default: ;
	endcase
end

endmodule