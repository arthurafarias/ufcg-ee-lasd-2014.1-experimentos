module  Datapath(
	output [3:0] FLAG_out, // Flag Bus
	output [15:0] A_bus, // Address out Bus
	output [15:0] D_bus, // Data out Bus
	input [15:0] D_in,  // Data in bus
	input [10:0] CNTRL_in, // Instruction Bus
	input [15:0] CONST_in, // Constant In Bus
	input CLK
);

parameter WORD_WIDTH = 16;

wire MB, MD, RW; wire [2:0] DA, BA, AA; wire [3:0] FS;
assign {DA, AA, BA, MB, FS, MD, RW} = CNTRL_in;

wire [WORD_WIDTH:0] RF0_out_B, BMD_out, FU0_out;

Datapath_RegisterFile #(WORD_WIDTH)	RF0(A_bus, RF0_out_B, BMD_out, AA, BA, DA, RW, CLK);
Datapath_BusMuxer		 #(WORD_WIDTH) BMB(D_bus, RF0_out_B, CONST_in, MB);
Datapath_BusMuxer		 #(WORD_WIDTH) BMD(BMD_out, FU0_out, D_in, MD);

Datapath_FunctionUnit #(WORD_WIDTH) FU0(
	.F(FU0_out),
	.V(FLAG_out[3]),
	.C(FLAG_out[2]),
	.N(FLAG_out[1]),
	.Z(FLAG_out[0]),
	.A(A_bus),
	.B(D_bus),
	.FS(FS)
);

endmodule