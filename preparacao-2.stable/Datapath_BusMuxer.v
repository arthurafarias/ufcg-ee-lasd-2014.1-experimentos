module Datapath_BusMuxer(
	output reg [WORD_WIDTH-1:0] Output,
	input [WORD_WIDTH-1:0] Bus_A, Bus_B,
	input S
	);

parameter WORD_WIDTH = 16;

always@(*)
	case(S)
		1'b0: Output = Bus_A;
		1'b1: Output = Bus_B;
	endcase
	
endmodule
