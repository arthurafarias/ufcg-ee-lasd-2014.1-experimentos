module Datapath_FunctionUnit_ArithmeticLogicUnit_Behavioral(F, C, A, B, FS);

parameter WORD_WIDTH = 16;

output reg [WORD_WIDTH-1:0] F;
output reg C;
input [WORD_WIDTH-1:0] A, B;
input [3:0] FS;

always@(*)
	case(FS)
		4'b0000: {C, F} = A;		// Transfer
		4'b0001: {C, F} = A+1;		// Increment
		4'b0010: {C, F} = A+B;		// Add
		4'b0011: {C, F} = A+B+1;	// (Unused)
		4'b0100: {C, F} = A+(~B);	// (Unused)
		4'b0101: {C, F} = A+(~B)+1;	// Subtraction
		4'b0110: {C, F} = A-1;		// Decrement
		4'b0111: {C, F} = A;		// (Unused)
		4'b1000: {C, F} = A&B;		// Bitwize and
		4'b1001: {C, F} = A|B;		// Bitwize or
		4'b1010: {C, F} = A^B;		// Bitwize xor
		4'b1011: {C, F} = (~A);		// Bitwize Invert
		4'b1100: {C, F} = B;		// Move B
		4'b1101: {C, F} = (B>>1);	// Shift Right B
		4'b1110: {C, F} = (B<<1);	// Shift Left B
		4'b1111: {C, F} = (~B);		// (Unused)
		default:;
	endcase

endmodule