module Datapath_FunctionUnit( F, V, C, N, Z, A, B, FS );
/*
* F - Function unit output data
* A - Function unit input data A
* B - Function unit input data B
* Z - Constant input data
* FS - Function unit function selector
* N - Negative Output Flag
* Z - Zero Flag
* C - Carry Flag - Overflow without signal
* V - Overflow Flag - Overflow with signal
*/

parameter WORD_WIDTH = 16;

output [WORD_WIDTH-1:0] F;
output V, C, N, Z;
input [WORD_WIDTH-1:0] A;
input [WORD_WIDTH-1:0] B;
input [3:0] FS;

//assign V = A[WORD_WIDTH-1]^B[WORD_WIDTH-1]^F[WORD_WIDTH-1];
//assign V = C^(((~A[14])&(B[14]^F[14]))|(A[14]&(~F[14])));

wire [WORD_WIDTH-1:0] V_temp;
assign V_temp = A[WORD_WIDTH-2:0]+B[WORD_WIDTH-2:0];
assign V = V_temp[WORD_WIDTH-1];
assign N = F[WORD_WIDTH-1];
assign Z = (!F)?1:0;

Datapath_FunctionUnit_ArithmeticLogicUnit_Behavioral #(WORD_WIDTH) ULA0(F,C,A,B,FS);
	
endmodule
