module Mod_Teste(
	output [6:0] HEX0,output [6:0] HEX1, output [6:0] HEX2, output [6:0] HEX3, output [6:0] HEX4, output [6:0] HEX5, output [6:0] HEX6, output [6:0] HEX7,
	output [8:0] LEDG,
	output [17:0] LEDR
	input CLOCK_27,
	input CLOCK_50,
	input [3:0] KEY,
	input [17:0] SW,
	);

parameter WORD_WIDTH = 4;

wire [WORD_WIDTH-1:0] B2HEXA_in, B2HEXB_in, B2HEXA_out, B2HEXB_out;

Decoder_Binary2HexSevenSegments B2HEXA[WORD_WIDTH-1:0] (
	.out(HEX0),
	.in(B2HEXA_in)
	);

Decoder_Binary2HexSevenSegments B2HEXB[2*WORD_WIDTH-1:WORD_WIDTH] (
	.out(HEX1),
	.in(B2HEXB_in)
	);

assign LEDR[2*WORD_WIDTH-1:0] = {B2HEXB_in,B2HEXA_in};

Datapath #(WORD_WIDTH) DP(
	.FLAG_out(LEDR[17:13]),
	.A_bus(B2HEXA_in[0]),
	.D_bus(B2HEXB_in[1]),
	.D_in(SW[7:4]),
	.CNTRL_in(SW[17:2]),
	.CONST_in({{(WORD_WIDTH-2){SW[3]}},SW[2:1]}),
	.CLK(SW[0])
	);

endmodule


	
